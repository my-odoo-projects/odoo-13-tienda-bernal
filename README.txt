Hay dos maneras, c�mo no, de instalar Odoo 13 (que es la versi�n que utilizo)
1. La primera manera, que es la que voy a tratar aqu�, es instalar Odoo directamente
2. La segunda manera es en un entorno virtual

Para instalarlo directamente estoy utilizando un Entorno Windows, espec�ficamente Windows 7.
Descargu� el instalador, el cual trae consigo la versi�n de PostgreSQL recomendada
No he necesitado instalar Python previamente, es, lo que podr�amos llamar una versi�n limpia del SO.
El servicio siempre est� en ejecuci�n, no necesito de comandos especiales. Por ejemplo para reiniciar el servidor [1]:
1. Ir a Inicio/Servicios
2. Buscar el servidor de Odoo, por ejemplo "odoo-server-13.0"
3. Seleccionar la opci�n, ya sea Reiniciar/Iniciar/Parar el servicio.

--- Creando un m�dulo con el comando Scaffold [2] ---
En el cmd (no es necesario que sea como Administrador):
<ubicaci�n del archivo python> +
<ubicaci�n del archivo bin> + scaffold + 
<nombre del m�dulo> + <nombre de la carpeta donde se va a crear el m�dulo>

Ejemplo:
"C:\Program Files (x86)\Odoo 13.0\python\python.exe" "C:\Program Files (x86)\Odoo 13.0\server\odoo-bin" scaffold mi_modulo "C:\Users\Ronnie\Desktop\Odoo_Modulos"

Fuentes:
[1] https://www.odoo.com/es_ES/forum/ayuda-1/how-to-start-stop-openerp-server-on-windows-28171
[2] https://youtu.be/SIOFgFfZNe4
